package sk.murin.howmanysecondskotlin

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    fun convert(view: View) {
        val input = edit_text_input.text.toString()
        if (input.length > 10 || input.isEmpty()) {
            Toast.makeText(this, "Príliš vela/málo číslic", Toast.LENGTH_LONG).show()
        } else {
            val inputSeconds = input.toInt()

            val days = (inputSeconds / 86400).toLong()
            val hours = (inputSeconds / 3600 % 24).toLong()
            val minutes = (inputSeconds / 60 % 60).toLong()
            val seconds = (inputSeconds % 60).toLong()

            tv_days_calculated.text = days.toInt().toString()
            tv_hours_calculated.text = hours.toInt().toString()
            tv_minutes_calculated.text = minutes.toInt().toString()
            tv_seconds_calculated.text = seconds.toInt().toString()
        }


    }

    fun reset(view: View) {
        tv_days_calculated.text = ""
        tv_hours_calculated.text = ""
        tv_minutes_calculated.text = ""
        tv_seconds_calculated.text = ""
        edit_text_input.setText("")
    }
}